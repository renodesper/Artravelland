import React from 'react';
import {Text, View} from 'react-native';

class Place extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Place'
  };

  render() {
    return (
      <View>
        <Text>Place Component</Text>
      </View>
    )
  }
}

export default Place;
