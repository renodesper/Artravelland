import React from 'react';
import {StatusBar, StyleSheet, View} from 'react-native';

class StatusBarBackground extends React.Component {
  render() {
    return (
      <View
        style={[ styles.statusBarBackground, this.props.style || {} ]}>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  statusBarBackground: {
    height: Expo.Constants.statusBarHeight,
    backgroundColor: 'rgba(177, 218, 255, 1)'
  }
})

export default StatusBarBackground;